package app;

public interface IPlayer
{
    public int getNextMove();
}